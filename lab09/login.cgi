#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

print header, start_html('Login');
warningsToBrowser(1);
print header, h1('Credit Card Validation');

$username = param('username') || '';
$password = param('password') || '';

chomp $username;
chomp $password;
# sanitize username
$username = substr $username, 0, 256;
$username =~ s/\W//g;

if ($username && $password) {	# have username and password
	my $password_file = "../accounts/$username/password";
	if (!open F, "<$password_file") {	# username is wrong
		print "Unknown username!\n";
	} else {
		$correct_password = <F>;
		chomp $correct_password;
		if ($password eq $correct_password) {	# username and password are both right
		print "$username authenticated.\n";
		} else {	# password is wrong
		    print "Incorrect password!\n";
		}
	}
} else {	# need more information
    print start_form, "\n";
    if (!$username && !$password) {	# need both username and password
    	print "Username:\n", textfield('username'), "\n";
   		print "Password:\n", textfield('password'), "\n";
    } elsif (!$username) {	# need username
    	print hidden(password => "$password"), "\n";
    	print "Username:\n", textfield('username'), "\n";
    } elsif (!$password) {	# need password
   		print hidden(username => "$username"), "\n";
   		print "Password:\n", textfield('password'), "\n";
   	}
   	
    print submit(value => Login), "\n";
    print end_form, "\n";
}
print end_html;
exit(0);