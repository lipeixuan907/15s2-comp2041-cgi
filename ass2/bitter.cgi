#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;


# Functions


sub main() {
    # define some global variables
    $debug = 0;
    $dataset_size = "medium"; 
    $users_dir = "dataset-$dataset_size/users";
    $bleats_dir = "dataset-$dataset_size/bleats";
    $profile_icon = "profile_icon.jpg";

    $login_user = param('login_user');
    $search_user_keyword = param('search_user_keyword');
    $search_bleats_keyword = param('search_bleats_keyword');
    $show_profile = param('show_profile');
    $send_bleat_text = param('send_bleat_text');
    $reply_bleat_text = param('reply_bleat_text');
    $reply_bleat_id = param('reply_bleat_id');
    $show_relevant_bleats = param('show_relevant_bleats');
    $unlisten_user = param('unlisten');
    %listens_list = &get_listen_list();
    $listen_to = param('listen_to');
    $btn_log_out = start_form({style=>"text-align: -webkit-center"}) . submit(-value => 'Logout', -class=>'bitter_home_button') . end_form;
    $btn_home = start_form({style=>"text-align: right; padding-right: 5%;"}) . submit(-value => 'Home', -class=>'bitter_home_button') . hidden(-name=>'login_user', -value=>$login_user) . end_form if $login_user;
    $home_logout = table({style=>'padding-left: 86%;'}, td($btn_home), td($btn_log_out));
    # print start of HTML ASAP to assist debugging if there is an error in the script
    print page_header();
    
    # Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    ## listen to a user
    if ($listen_to) {
        &listen_to_user($listen_to);
    }
    ## unlisten a user
    if ($unlisten_user) {
        &unlisten_to_user($unlisten_user);
    }
    # reply bleat
    if ($reply_bleat_text) {
        $send_bleat_text = $reply_bleat_text;
        &send_bleat(param('reply_bleat_id'));
    }
    if ($search_user_keyword) {
        &search_user();
    } elsif ($search_bleats_keyword) {
        &search_bleats();
    } else {
        if ($login_user) {
            if ($show_profile) {    # print another user page
                print user_page($show_profile);
            } else {    # print user home page
                # send bleat
                if (!$reply_bleat_text && $send_bleat_text) {
                    &send_bleat();
                }
                print user_page();
            }
        	
        } else {
        	login_page();
        }   
    }
    print page_trailer();
}
# listen to a user
sub listen_to_user($) {
    my $new_user = $_[0];
    %listens_list = &get_listen_list();
    return if exists $listens_list{$new_user}; # return if already listened
    my @text = ();
    my $flag = 0;
    open my $d, "$users_dir/$login_user/details.txt";
    while ((my $line = <$d>)) {
        chomp $line;
        if ($line =~ /^listens: (.*)$/) {
            $flag = 1;
            push @text, "$line $new_user\n";
        } else {
            push @text, "$line\n" 
        }
    }
    push @text, "listens: $new_user\n" if $flag == 0;
    close $d;
    open (my $w, "> $users_dir/$login_user/details.txt");
    print $w @text;
    close $w;
    $listens_list{$new_user} = 1;
}
# unlisten to a user
sub unlisten_to_user {
    my $the_user = $_[0];
    %listens_list = &get_listen_list();
    return if !exists $listens_list{$the_user}; # return if already unlistened
    my @text = ();
    my @listens = ();
    push @listens, 'listens:';
    open my $d, "$users_dir/$login_user/details.txt";
    while (my $line = <$d>) {
        chomp $line;
        if ($line =~ /^listens: (.*)$/) {
            my $users = $1;
            my @user_list = ($users =~ /\S+/g);
            while (scalar @user_list) {
                my $user = shift @user_list;
                push @listens, " $user" if $user ne $unlisten_user; 
            }
            push @text, "\n";
        } else {
            push @text, "$line\n";
        }
    }
    push @text, "\n";
    push @text, @listens;
    push @text, "\n";
    close $d;
    open (my $w, "> $users_dir/$login_user/details.txt");
    print $w @text;
    close $w;
    delete $listens_list{$unlisten_user};
}

# get listens list
sub get_listen_list {
    my %listen_list = ();
    open my $d, "$users_dir/$login_user/details.txt";
    while (my $line = <$d>) {
        chomp $line;
        if ($line =~ /^listens: (.*)$/) {
            my @users = ($1 =~ /\S+/g);
            foreach my $user (@users){
                $listen_list{$user} = 1;
            }
        }
    }
    close $d;
    return %listen_list;
}

# send bleat
sub send_bleat {
    $send_bleat_text = substr $send_bleat_text, 0, 142;   # limit 142 characters
    my $reply_to = '';
    my $reply_bleat_id = $_[0];
    my $reply_to = 'in_reply_to: ' . $reply_bleat_id . "\n" if $reply_bleat_id;
    my $time = time;
    my $bleat_number = '1'.time;
    # create a bleat file
    open(my $f, "> $bleats_dir/$bleat_number");
        print $f "$reply_to"
                ."username: $login_user\n"
                ."time: $time\n"
                ."bleat: $send_bleat_text\n";
    close $f;

    # add to bleats.txt
    open(my $b, ">> $users_dir/$login_user/bleats.txt");
        print $b "$bleat_number\n";
    close $b;
}

# get reply textfiled and button
sub get_reply_bleat($) {
    my $the_bleatsID = $_[0];
    my $btn_reply_bleat = submit(-value => 'Reply', -class=>'bitter_logout_button');
    my $send_bleat = start_form
                . '&nbsp;&nbsp;&nbsp;&nbsp;'
                . textfield(-name=>'reply_bleat_text')
                . $btn_reply_bleat
                . '<input type="hidden" name="reply_bleat_id" value= "' . $the_bleatsID . '"  />'
                . hidden(-name=>'show_profile', -value=>param('show_profile'))
                . hidden(-name=>'show_relevant_bleats', value=>"$show_relevant_bleats")
                . hidden(-name=>'login_user', -value=>$login_user)
                . hidden(-name=>'search_bleats_keyword', -value=>param('search_bleats_keyword')) 
                . end_form;
}

# user searching page
sub search_user {
    my @users = sort(glob("$users_dir/*"));
    foreach my $user (sort @users) {
        $user =~ /^$users_dir\/(.*)$/;
        my $this_username = $1;
        if ($this_username =~ /$search_user_keyword/i) { # match username
            my $this_user_info = &get_user_info($this_username);
            push @user_search_result, $this_user_info;
            # push @user_search_result, $this_username;
        } else {    # match fullname
            open my $d, "$user/details.txt" or die "can not open $details_filename: $!";
            while (my $line = <$d>) {
                chomp $line;
                if ($line =~ /^full_name: (.*$search_user_keyword.*)$/i) {
                    my $this_user_info = &get_user_info($this_username);
                    push @user_search_result, $this_user_info;
                }
            }
            close $d;
        }
    }
    print table(Tr(h2({style=>"color: white; padding-left:5%"}, "Searching result for: $search_user_keyword")), Tr(p(@user_search_result)));
    
}
sub get_user_info($) {
    my $this_username = $_[0];
    open my $d, "$users_dir/$this_username/details.txt" or die "can not open $details_filename: $!";
    my $this_user_image = &get_small_profile_image($this_username);
    my $this_user_fullname;
    while (my $line = <$d>) {
        chomp $line;
        if ($line =~ /^full_name: (.*)$/) {
            $this_user_fullname = $1;
            last;
        }
    }
    my $this_user_names = table({style=>"padding-left:5%"},Tr({style=>"color: white"},td("Username:"), td($this_username)),Tr({style=>"color: white"},td("Full Name:"), td($this_user_fullname)));
    my $btn_show_detail = start_form 
                        . submit(-value => 'Detail', -class=>'bitter_logout_button') 
                        . hidden(-name=>'show_profile', -value=>$this_username)
                        . hidden(-name=>'login_user', -value=>$login_user)
                        . end_form;
    return table({style=>"padding-left:15%"},td($this_user_image), td($this_user_names), td($btn_show_detail));
}

## bleats serching page
sub search_bleats {
    my @search_bleats_result_list = ();
    my %bleats_details = &get_bleats_details();

    

    my @bleat_files = sort(glob("$bleats_dir/*"));
    foreach my $bleat_file (@bleat_files) {
        # open my $bf, $bleat_file or die "can not open $bleat_file: $!";
        $bleat_file =~ /^$bleats_dir\/(.*)$/;
        $bleats_id = $1;
        push @search_bleats_result_list, $bleats_id if $bleats_details{$bleats_id}{bleat} =~ /^.*$search_bleats_keyword.*$/gi;
    }
    my $title_result = table (Tr(h2("Searching result for: $search_bleats_keyword")), &get_bleats_in_order(@search_bleats_result_list));
    print table(td(&get_side_bar), td({-class=>'bitter_user_bleats'}, $title_result));
}


sub login_page {
	my $username = param('username') || '';
	my $password = param('password') || '';
	chomp $username;
	chomp $password;
	# sanitize username
	$username = substr $username, 0, 256;
	$username =~ s/\W//g;

	if ($username && $password) {	# have username and password
		my $details_file = "$users_dir/$username/details.txt";
		if (-e $details_file) {
			open my $d, "$details_file", or die "can not open $details_filename: $!";
			while (my $line = <$d>) {
				chomp $line;
				# findout password
				if ($line =~ /^password: (.*)$/) {
					if ($password eq $1) {	# password correct
						print hidden(-name=>login_user, -value=>$username);
						$login_user = $username;
						print &user_page();
					} else {	# password incorrect
						print start_form(-class=>"bitter_login");
						print h2("Incorrect username or password, please try again!");
						print submit(-class=>'bitter_button', -value=>'Back');
						print end_form, "\n";
					}
				}
			}
		} else {	# username incorrect
			print start_form(-class=>"bitter_login");
			print h2("Incorrect username or password, please try again!");
			print submit(-class=>'bitter_button', -value=>'Back');
			print end_form, "\n";
		}

	} else {	# need more information
	    print start_form(-class=>"bitter_login");
	    print h2("Please log in"), "\n";
	    if (!$username && !$password) {	# need both username and password
	    	print "Username:\n", textfield('username'), "\n";
	   		print "Password:\n", password_field('password'), "\n";
	    } elsif (!$username) {	# need username
	    	print hidden(password => "$password"), "\n";
	    	print "Username:\n", textfield('username'), "\n";
	    } elsif (!$password) {	# need password
	   		print hidden(username => "$username"), "\n";
	   		print "Password:\n", textfield('password'), "\n";
	   	}
	   	
	    print submit(-value => Login, -class=>'bitter_button'), "\n";
	    print end_form, "\n";
	}

}

sub get_profile_image($) {
    my $username = $_[0];
    my $image_file = "$users_dir/$username/profile.jpg";
    my $profile_image;
    if (-e $image_file) {
        $profile_image = img{-class=>'bitter_user_image', -src=>$image_file, -align=>'Profile'};
    } else {
        $profile_image = img{-class=>'bitter_user_image', -src=>$profile_icon, -align=>'Profile'};
    }
}
sub get_small_profile_image($) {
    my $username = $_[0];
    my $image_file = "$users_dir/$username/profile.jpg";
    my $profile_image;
    if (-e $image_file) {
        $profile_image = img{-class=>'bitter_small_user_image', -src=>$image_file, -align=>'Profile'};
    } else {
        $profile_image = img{-class=>'bitter_small_user_image', -src=>$profile_icon, -align=>'Profile'};
    }
}
sub get_little_profile_image($) {
    my $username = $_[0];
    my $image_file = "$users_dir/$username/profile.jpg";
    my $profile_image;
    if (-e $image_file) {
        $profile_image = img{-class=>'bitter_little_user_image', -src=>$image_file, -align=>'Profile'};
    } else {
        $profile_image = img{-class=>'bitter_little_user_image', -src=>$profile_icon, -align=>'Profile'};
    }
}

sub get_side_bar {
    my @listens_to = ();
    %listens_list = &get_listen_list();
    foreach my $listened_user (sort keys %listens_list) {
        my $this_user_img = get_small_profile_image($listened_user);
        # my $btn_unlisten = submit(-value=> 'Unlisten', -class=>'bitter_logout_button');
        my $this_user = p($this_user_img.'<br>'.$listened_user.'<br>'.&get_unlisten_button($listened_user).'<br>');
        push @listens_to, $this_user;
    }
    return @listens_to;
}

sub get_bleats_details {
    my %bleats_details = ();
    my @bleat_files = sort(glob("$bleats_dir/*"));
    foreach my $bleat_file (@bleat_files) {
        open my $bf, $bleat_file or die "can not open $bleat_file: $!";
        $bleat_file =~ /^$bleats_dir\/(.*)$/;
        $bleats_id = $1;
        while (my $line = <$bf>) {
            chomp $line;
            $line =~ /^([^:]*): (.*)$/;
            my $key;
            my $value;
            ($key, $value) = ($1, $2);
            # $details{$key} = $value;
            $bleats_details{$bleats_id}{$key} = $value;
        }
        close $bf;
    }
    return %bleats_details;
}

sub get_listen_button($) {
    my $username = $_[0];
    my $btn_listen = submit(-value=> 'Listen', -class=>'bitter_logout_button');
    return start_form 
         . $btn_listen 
         . hidden(-name=>'listen_to', -value=>$username) 
         . hidden(-name=>'login_user', -value=>$login_user) 
         . hidden(-name=>'show_profile', -value=>param('show_profile')) 
         . hidden(-name=>'search_bleats_keyword', -value=>param('search_bleats_keyword')) 
         . hidden(-name=>'show_relevant_bleats', -value=>param('show_relevant_bleats'))
         . end_form;
}
sub get_unlisten_button($) {
    my $username = $_[0];
    my $btn_unlisten = submit(-value=> 'Unlisten', -class=>'bitter_logout_button');
    return start_form 
         . $btn_unlisten 
         . hidden(-name=>'unlisten', -value=>$username) 
         . hidden(-name=>'login_user', -value=>$login_user) 
         . hidden(-name=>'show_profile', -value=>param('show_profile')) 
         . hidden(-name=>'search_bleats_keyword', -value=>param('search_bleats_keyword')) 
         . hidden(-name=>'show_relevant_bleats', -value=>param('show_relevant_bleats'))
         . end_form;
}

sub get_bleats_in_order(@) {
    my @bleats_list = @_;
    my @bleats = ();
    my %bleats_details = &get_bleats_details();
    foreach my $bleatsID (sort {$bleats_details{$b}{time} <=> $bleats_details{$a}{time}} @bleats_list) {
        # Time
        # Username
        # In_reply_to
        # Position(latitude/longitude)
        # Bleat
        my $bleat_info;
        my $bleat_text;
        my $username = $bleats_details{$bleatsID}{username};
        my $small_profile_image = &get_small_profile_image($username);
        if ((!exists $listens_list{$username}) && ($username ne $login_user)) { # add listen button
            $small_profile_image .= '<br>' . &get_listen_button($username);
        } elsif (exists$listens_list{$username} && ($listens_list{$username} == 1)) {    # add unlisten button
            $small_profile_image .= '<br>' . &get_unlisten_button($username);
        }
        $bleat_info .= "\nTime:&nbsp;&nbsp;&nbsp;&nbsp;" . localtime($bleats_details{$bleatsID}{time}) if exists $bleats_details{$bleatsID}{time};
        $bleat_info .= "\nPosition:&nbsp;&nbsp;&nbsp;&nbsp;(" . $bleats_details{$bleatsID}{latitude} . "/" . $bleats_details{$bleatsID}{longitude} . ")" if exists $bleats_details{$bleatsID}{latitude};
        $bleat_info .= "\nUsername:&nbsp;&nbsp;&nbsp;&nbsp;" . $username if exists $bleats_details{$bleatsID}{username};
        $bleat_info = '<div class="bleats_info">' . $bleat_info . '</div>';
        $bleat_text .= "\nIn reply to:&nbsp;&nbsp;&nbsp;&nbsp;" . &get_little_profile_image($bleats_details{$bleats_details{$bleatsID}{in_reply_to}}{username}) . $bleats_details{$bleats_details{$bleatsID}{in_reply_to}}{bleat} if exists $bleats_details{$bleatsID}{in_reply_to};
        $bleat_text .= "<br>Bleat:&nbsp;&nbsp;&nbsp;&nbsp;" . $bleats_details{$bleatsID}{bleat} if exists $bleats_details{$bleatsID}{bleat};
        $bleat_text .= "\n";
        $bleat_text = '<div class="bleats_text">' . $bleat_text . '</div>';
        my $tableOfText = table(Tr($bleat_info), Tr($bleat_text), Tr(&get_reply_bleat($bleatsID)));
        
        my $table_of_info_text = table({class=>'bleats_table'},td($small_profile_image), td($tableOfText));
        push @bleats, $table_of_info_text;
    }
    return @bleats;
}

#
# Show unformatted details for user "n".
# Increment parameter n and store it as a hidden variable
#
sub user_page {
    if ($_[0]) {
        $user_to_show = "$users_dir/$_[0]";
        $username_to_show = $_[0];
    } else {
        $user_to_show = "$users_dir/$login_user";
        $username_to_show = $login_user;
    }
    
    
    ## image
    my $profile_image = &get_profile_image($username_to_show);

    ## details
    my $details_filename = "$user_to_show/details.txt";
    open my $detail_handle, "$details_filename" or die "can not open $details_filename: $!";
    # my %users = ();
    my %details = ();
    while (my $line = <$detail_handle>) {
        chomp $line;
        $line =~ /^([^:]*): (.*)$/;
        my $key;
        my $value;
        ($key, $value) = ($1, $2);
        $details{$key} = $value;
    }
    # $users{$details{username}} = \%details;

    $details = "    Username:    ".$details{username};
    $details .= "\n    Full Name:    ".$details{full_name} if exists $details{full_name};
    $details .= "\n    Home Suburb:    ".$details{home_suburb} if exists $details{home_suburb};
    $details .= "\n    Home Position(latitude/longitude):    (".$details{home_latitude} ."/". $details{home_longitude} . ")" if exists $details{home_latitude};
    $imageAndDetails = table({style=>"padding-left:19%"},Tr(td($profile_image),td($details)));
    close $detail_handle;

    
    ## listens to (side bar)
    @listens_to = &get_side_bar();
    

    ## bleats
    my @bleats_to_show = ();
    if ($show_relevant_bleats) { # show relevant bleats
        my $username = $show_relevant_bleats;
        ## Relevant Bleats
        $bleatsTitle = h1({style=>"text-align: center"}, "BLEATS");
        %bleats_details = ();
        my @bleat_files = sort(glob("$bleats_dir/*"));
        foreach my $bleat_file (@bleat_files) {
            open my $bf, $bleat_file or die "can not open $bleat_file: $!";
            $bleat_file =~ /^$bleats_dir\/(.*)$/;
            $bleats_id = $1;
            while (my $line = <$bf>) {
                chomp $line;
                my $this_line = $line;
                if ($line =~ /^username: $username$/) {   # send by login user
                    push @bleats_to_show, $bleats_id;
                }
                if ($line =~ /^username: (.*)$/) { # send by listened user
                    if ($listens_list{$1}) {
                        push @bleats_to_show, $bleats_id;
                    }
                }
                if ($line =~ /^bleat: .*\@$username.*$/) { # mentioned login user
                    push @bleats_to_show, $bleats_id;
                }
                if ($line =~ /^bleat: .*\#.*$/) { # mentioned login user
                    push @bleats_to_show, $bleats_id;
                }
                $this_line =~ /^([^:]*): (.*)$/;
                my $key;
                my $value;
                ($key, $value) = ($1, $2);
                # $details{$key} = $value;
                $bleats_details{$bleats_id}{$key} = $value;
            }
            close $bf;
        }
    } else {    # show user bleats
        my $bleats_filename = "$user_to_show/bleats.txt";
        open my $bleat_handle, "$bleats_filename" or die "can not open $details_filename: $!";
        %bleats_details = ();
        $bleatsTitle = h1({style=>"text-align: center"}, "BLEATS");
        while (my $line = <$bleat_handle>) {
            chomp $line;
                my $bleats_id = $line;
            my $bleats_details_file = "$bleats_dir/$bleats_id";
            open my $b_handle, "$bleats_details_file" or die "can not open $bleats_details_file: $!";
            # my $details = ();
            while(my $line = <$b_handle>) {
                chomp $line;
                $line =~ /^([^:]*): (.*)$/;
                my $key;
                my $value;
                ($key, $value) = ($1, $2);
                # $details{$key} = $value;
                $bleats_details{$bleats_id}{$key} = $value;
            }
            # $bleats_details{$bleats_id} = \%details;
            close $b_handle;
        }
        @bleats_to_show = keys %bleats_details;
    }
    

    # close $bleat_handle;
    # my $next_user = $n + 1;
    # <input type="hidden" name="n" value="$next_user">
    # <input type="submit" value="Next user" class="bitter_button">
    @bleats = &get_bleats_in_order(@bleats_to_show);

    

    ## searching
    $btn_search_user = submit(-value => 'Serach User', -class=>'bitter_logout_button');
    $btn_search_bleats = submit(-value => 'Serach Bleats', -class=>'bitter_logout_button');
    $search_user = start_form({style=>'padding-left:20%'}) . textfield(-name=>'search_user_keyword') . $btn_search_user . hidden(-name=>'search_user_keyword', -value=>param('search_user_keyword')) . hidden(-name=>'login_user', -value=>$login_user) . end_form;
    $search_bleats = start_form({style=>'padding-left:20%'}) . textfield(-name=>'search_bleats_keyword') . $btn_search_bleats . hidden(-name=>'search_bleats_keyword', -value=>param('search_bleats_keyword')) . hidden(-name=>'login_user', -value=>$login_user) . end_form;
    $searching_field = table(td($search_user), td($search_bleats));

    $function_board = table({class=>'bitter_function_board'}, td($searching_field), td({-style=>'color: white;font-size: large;font-style: italic;padding-left: 40%;'}, "Hello, $login_user"));

    ## send bleat

    my $btn_send_bleat = submit(-value => 'Send Bleat', -class=>'bitter_logout_button');
    my $btn_show_relevant_bleats = submit(-name=>'show_relevant_bleats', -value=>'Show Relevant Bleats', -class=>'bitter_logout_button');
    if (defined param('show_profile')) {
        $show_relevant_bleats = param('show_profile');
    } else {
        $show_relevant_bleats = $login_user;
    }
     
    $send_bleat = start_form
                . textarea(-name=>'send_bleat_text')
                . $btn_send_bleat
                . $btn_show_relevant_bleats
                . hidden(-name=>'send_bleat_text', -value=>param('send_bleat_text'))
                . hidden(-name=>'show_profile', -value=>param('show_profile'))
                . hidden(-name=>'show_relevant_bleats', value=>"$show_relevant_bleats")
                . hidden(-name=>'login_user', -value=>$login_user)
                . end_form; 
  
    return <<eof
<form method="POST" action="">
</form>

<div>$function_board</div>
<p>
<div class="bitter_user_details">
    $imageAndDetails
</div>
    <div style="text-align: right;padding-right: 6%;">
        $send_bleat
    </div>
<p>
<table>
    <td class="bitter_side_bar">
    @listens_to
    </td>
    <td class="bitter_user_bleats">
        $bleatsTitle
        <p>
        @bleats
    </td>
</table>
eof
}


#
# HTML placed at the top of every page
#
sub page_header {
    return <<eof
Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
<title>Bitter</title>
<link href="bitter.css" rel="stylesheet">
</head>
<body bgcolor="#0080FF">
<div class="bitter_heading">Welcome to Bitter</div>
$home_logout
eof
}


#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer {
    my $html = "";
    $html .= join("", map("<!-- $_=".param($_)." -->\n", param())) if $debug;
    $html .= end_html;
    return $html;
}

main();