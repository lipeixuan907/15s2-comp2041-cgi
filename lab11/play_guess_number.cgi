#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);

print header, start_html("A Guessing Game Player"), "\n";
warningsToBrowser(1);

$lower_limit = param('lower_limit') || 1;
$uper_limit = param('uper_limit') || 100;
$guess_number = param('guess_number') || 50;

if (defined param('correct')) {	# Correct
	print start_form, "\n";
	print "I win!!!! ", submit(-name=>'result', -value=>'Play Again'), "\n";
	print end_form;
} else {	# not correct
	if (defined param('higher')) {	# Higher
		$lower_limit = $guess_number + 1;
	} elsif (defined param('lower')) {	# Lower
		$uper_limit = $guess_number - 1;
	}
	$guess_number = int(($uper_limit + $lower_limit) / 2);
	param('lower_limit', $lower_limit);
	param('uper_limit', $uper_limit);
	param('guess_number', $guess_number);
	print start_form,
		  "My guess is: $guess_number ", "\n",
		  submit(-name=>'higher', -value=>'Higher?'), "\n",
		  submit(-name=>'correct', -value=>'Correct?'), "\n",
		  submit(-name=>'lower', -value=>'Lower?'), "\n",
		  hidden('lower_limit'), "\n",
		  hidden('uper_limit'), "\n",
		  hidden('guess_number'), "\n",
		  end_form, "\n";
}

print end_html;
exit 0;