#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);

$max_number_to_guess = 99;

print <<eof;
Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Guess A Number</title>
    <link href="guess_number.css" rel="stylesheet">
</head>
<body>
eof

warningsToBrowser(1);

$number_to_guess = param('number_to_guess');
$guess = param('guess');

$game_over = 0;

if (defined $number_to_guess and defined $guess) {
    $guess =~ s/\D//g;
    $number_to_guess =~ s/\D//g;
    if ($guess == $number_to_guess) {
        print p({-class=>'guess_number_info'}, "You guessed right, it was $number_to_guess.\n");
        $game_over = 1;
    } elsif ($guess < $number_to_guess) {
        print p({-class=>'guess_number_info'}, "Its higher than $guess.\n");
    } else {
        print p({-class=>'guess_number_info'}, "Its lower than $guess.\n");
    }
} else {
    $number_to_guess = 1 + int(rand $max_number_to_guess);
    print p({-class=>'guess_number_info'}, "I've  thought of number 0..$max_number_to_guess");
}

if ($game_over) {
print <<eof;
    <form method="POST" action="" style=text-align:center>
        <input type="submit" value="Play Again" class="guess_number_button">
    </form>
eof
} else {
print <<eof;
    <form method="POST" action="" style=text-align:center>
        <input type="textfield" name="guess" class="guess_number_textfield">
        <input type="hidden" name="number_to_guess" value="$number_to_guess">
    </form>
eof
}

print <<eof;
</body>
</html>
eof