#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

print header, start_html("Credit Card Validation"), "\n";
warningsToBrowser(1);
print h2('Credit Card Validation'), "\n";

our $flag_close = 0;
our $result = 0;
my $credit_card = param('credit_card');
$credit_card =~ s/\</&lt;/g;
$credit_card =~ s/\>/&gt;/g;
my $action = param('action');
chomp $credit_card;
our $orginal_credit_card = $credit_card;

# page 4
if (defined $action && $action eq 'Close') {	# when push the close button
	print "Thank you for using the Credit Card Validator\n";
}

# page 1, 2, 3
else {
	print start_form, "\n";
	print p("This page checks whether a potential credit card number satisfies the Luhn Formula.\n");
	
	if (defined $credit_card) {
		$result = validate($credit_card);
		if ($result == 0) {	# last number is valid
			print p("$orginal_credit_card is valid"), "\n";
			print "Another card number: ", "\n";
			print textfield(-name => 'credit_card', -override => 1), "\n";	# clean the text field
		} elsif ($result == 1) {	# last number is invalid, does not contain exactly 16 digits
			print span(font({style => "color: red; font-weight: bold"}, "$orginal_credit_card is invalid - does not contain exactly 16 digits")), "\n";
			print p;
			print "Try again: ", "\n";
			print textfield(-name => 'credit_card'), "\n";
		} elsif ($result == 2) {	# last number is invalid
			print span(font({style => "color: red; font-weight: bold"}, "$orginal_credit_card is invalid")), "\n";
			print p;
			print "Try again: ", "\n";
			print textfield(-name => 'credit_card'), "\n";
		}
	} else{	# first time to use
		print "Enter credit card number: ", "\n";
		print textfield(-name => 'credit_card', -override => 1), "\n";	# clean the text field
	}
	
	# print hidden(credit_card => "$credit_card"), "\n";
	print submit(-name => 'action', -value => 'Validate'), "\n";
	print reset, "\n";
	print submit(-name => 'action', -value => 'Close'), "\n";
	print end_form, "\n";
}

print end_html;
exit 0;

# functions

sub validate($) {
	my $card_number = $_[0];
	$card_number =~ s/\D//g;
	my @digits = split //, $card_number;
	if (scalar @digits != 16) {	# invalid, does not contain exactly 16 digits
		return 1;
	} else {	# has exactly 16 digits
		my $result = &luhn_checksum(@digits);
		if ($result % 10 == 0) {
			$flag_last_status = 1;
			return 0;	# valid
		} else {
			return 2;	# invalid
		}
	}
}

sub luhn_checksum(@) {
	my @digits = @_;
	@digits = reverse(@digits);
	my $checksum = 0;
	for (my $i = 1; $i <= 16; $i++) {
		my $weight = 1;
		$weight = 2 if $i % 2 == 0;
		my $d = $digits[$i-1] * $weight;
		$d -= 9 if $d > 9;
		$checksum += $d;
	}
	return $checksum;
}