#!/usr/bin/perl -w

use strict;

my @card_numbers = @ARGV;
foreach my $card_number (@card_numbers) {
	my $message = &validate($card_number);
	print "$message\n";
}

sub validate($) {
	my $card_number = $_[0];
	$card_number =~ s/\D//g;
	my @digits = split //, $card_number;
	if (scalar @digits != 16) {	# does not contain exactly 16 digits
		return "$_[0] is invalid - does not contain exactly 16 digits";
	} else {	# has exactly 16 digits
		my $result = &luhn_checksum(@digits);
		if ($result % 10 == 0) {
			return "$_[0] is valid";
		} else {
			return "$_[0] is invalid";
		}
	}
}

sub luhn_checksum(@) {
	my @digits = @_;
	@digits = reverse(@digits);
	my $checksum = 0;
	for (my $i = 1; $i <= 16; $i++) {
		my $weight = 1;
		$weight = 2 if $i % 2 == 0;
		my $d = $digits[$i-1] * $weight;
		$d -= 9 if $d > 9;
		$checksum += $d;
	}
	return $checksum;
}